upstream rails_app {  
   server app:3000;
}
server {
   listen 443 ssl;
   # define your domain  
   server_name www.cosorg.fr cosorg.fr;
   # define the public application root  
   root   $RAILS_ROOT/public;  
   index  index.html;
   # define where Nginx should write its logs  
   access_log $RAILS_ROOT/log/nginx.access.log;  
   error_log $RAILS_ROOT/log/nginx.error.log;

   # RSA certificates
   ssl_certificate      /etc/nginx/certs/live/cosorg.fr/fullchain.pem;
   ssl_certificate_key  /etc/nginx/certs/live/cosorg.fr/privkey.pem;

   # Improve HTTPS performance with session resumption
   ssl_session_cache shared:SSL:10m;
   ssl_session_timeout 10m;

   # Enable server-side protection against BEAST attacks
   ssl_protocols TLSv1.2;
   ssl_prefer_server_ciphers on;
   ssl_ciphers "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384";

   # Aditional Security Headers
   # ref: https://developer.mozilla.org/en-US/docs/Security/HTTP_Strict_Transport_Security
   add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

   # ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
   add_header X-Frame-Options DENY always;

   # ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
   add_header X-Content-Type-Options nosniff always;

   # https://www.owasp.org/index.php/List_of_useful_HTTP_headers
   #add_header X-XSS-Protection "1; mode=block";


   # Enable OCSP stapling
   # ref. http://blog.mozilla.org/security/2013/07/29/ocsp-stapling-in-firefox
   ssl_stapling on;
   ssl_stapling_verify on;
   ssl_trusted_certificate /etc/nginx/certs/live/cosorg.fr/fullchain.pem;
   resolver 1.1.1.1 1.0.0.1 [2606:4700:4700::1111] [2606:4700:4700::1001] valid=300s; # Cloudflare
   resolver_timeout 5s;

   #location ^~ /.well-known/acme-challenge {
   #   allow all;
   #   root /var/www/acme_challenge_webroot;
   #   default_type text/plain;
   #}
  
   # deny requests for files that should never be accessed  
   location ~ /\. {    
      deny all;  
   }
   location ~* ^.+\.(rb|log)$ {    
      deny all;  
   }  
 
   # serve static (compiled) assets directly if they exist (for rails production)  
   location ~ ^/(assets|images|javascripts|stylesheets|swfs|system)/   {    
      try_files $uri @rails;     
      access_log off;    
      gzip_static on; 
      # to serve pre-gzipped version     
      expires max;    
      add_header Cache-Control public;     
      
      add_header Last-Modified "";    
      add_header ETag "";    
      break;  
   } 
  
   # send non-static file requests to the app server  
   location / {    
      try_files $uri @rails;  
   }   
   location @rails {    
      proxy_set_header  X-Real-IP  $remote_addr;    
      proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header  X-Forwarded-Proto $scheme;
      proxy_set_header Host $http_host;    
      proxy_redirect off;    
      proxy_pass http://rails_app;  
   }
}
server {
    listen 80;
    server_name www.cosorg.fr cosorg.fr;
    rewrite ^ https://$host$request_uri? permanent;
}

#COSCRM APP
upstream rails_crm {
   server app:3001;
}
server {
   listen 443 ssl;
   # define your domain
   server_name crm.cosorg.fr;
   # define the public application root
   #root   $RAILS_ROOT/public;
   index  index.html;
   # define where Nginx should write its logs
   access_log $RAILS_ROOT/log/nginx.access.log;
   error_log $RAILS_ROOT/log/nginx.error.log;

   # RSA certificates
   ssl_certificate      /etc/nginx/certs/live/crm.cosorg.fr/fullchain.pem;
   ssl_certificate_key  /etc/nginx/certs/live/crm.cosorg.fr/privkey.pem;

   # Improve HTTPS performance with session resumption
   ssl_session_cache shared:SSL:10m;
   ssl_session_timeout 10m;

   # Enable server-side protection against BEAST attacks
   ssl_protocols TLSv1.2;
   ssl_prefer_server_ciphers on;
   ssl_ciphers "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384";

   # Aditional Security Headers
   # ref: https://developer.mozilla.org/en-US/docs/Security/HTTP_Strict_Transport_Security
   add_header Strict-Transport-Security "max-age=31536000; includeSubDomains";

   # ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
   add_header X-Frame-Options DENY always;

   # ref: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
   add_header X-Content-Type-Options nosniff always;

   # https://www.owasp.org/index.php/List_of_useful_HTTP_headers
   #add_header X-XSS-Protection "1; mode=block";


   # Enable OCSP stapling
   # ref. http://blog.mozilla.org/security/2013/07/29/ocsp-stapling-in-firefox
   ssl_stapling on;
   ssl_stapling_verify on;
   ssl_trusted_certificate /etc/nginx/certs/live/crm.cosorg.fr/fullchain.pem;
   resolver 1.1.1.1 1.0.0.1 [2606:4700:4700::1111] [2606:4700:4700::1001] valid=300s; # Cloudflare
   resolver_timeout 5s;

   #location ^~ /.well-known/acme-challenge {
   #   allow all;
   #   root /var/www/acme_challenge_webroot;
   #   default_type text/plain;
   #}

   # deny requests for files that should never be accessed
   location ~ /\. {
      deny all;
   }
   location ~* ^.+\.(rb|log)$ {
      deny all;
   }

   # serve static (compiled) assets directly if they exist (for rails production)
   location ~ ^/(assets|images|javascripts|stylesheets|swfs|system)/   {
      try_files $uri @rails;
      access_log off;
      gzip_static on;
      # to serve pre-gzipped version
      expires max;
      add_header Cache-Control public;

      add_header Last-Modified "";
      add_header ETag "";
      break;
   }

   # send non-static file requests to the app server
   location / {
      try_files $uri @rails;
   }
   location @rails {
      proxy_set_header  X-Real-IP  $remote_addr;
      proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header  X-Forwarded-Proto $scheme;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      proxy_pass http://rails_crm;
   }
}
server {
    listen 80;
    server_name crm.cosorg.fr;
    rewrite ^ https://$host$request_uri? permanent;
}
