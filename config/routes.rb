Rails.application.routes.draw do

  get 'errors/not_found'
  get 'errors/internal_server_error'
  resources :orders do
    member do
      get :add_item
      get :remove_item
    end
    resources :items do
    end
  end
  match '/404', :to => 'errors#not_found', via: [:get, :post]
  match '/422', :to => 'errors#unprocessable_entity', via: [:get, :post]
  match '/500', :to => 'errors#internal_server_error', via: [:get, :post]

  post '/charges' => 'orders#charges', as: :stripe_charges
  get '/payment' => 'orders#payment', as: :stripe_payment
  devise_for :users
  resources :users
  resources :tickets
  root to: 'events#home'

  resources :exhibitors

  resources :events, except: [:index] do
    resources :exhibitors, only: :new
    resources :tickets
  end

  resources :angles
  resources :activities

  namespace :api do
    resources :events, only: [:index]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
