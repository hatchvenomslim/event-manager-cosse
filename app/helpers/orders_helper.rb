module OrdersHelper

  def status(order)
    if order.status.nil?
      "pending"
    else
      order.status
    end
  end
end
