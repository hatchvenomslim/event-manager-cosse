json.extract! item, :id, :first_name, :last_name, :email, :order_id, :event_id, :created_at, :updated_at
json.url item_url(item, format: :json)
