json.extract! order, :id, :user_id, :ticket_id, :transaction_id, :amount, :event_id, :quantity, :created_at, :updated_at
json.url order_url(order, format: :json)
