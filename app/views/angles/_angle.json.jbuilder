json.extract! angle, :id, :name, :pricing, :state, :created_at, :updated_at
json.url angle_url(angle, format: :json)
