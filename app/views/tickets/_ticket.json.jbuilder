json.extract! ticket, :id, :title, :event_id, :price, :max_per_person, :min_per_person, :quantity, :sold, :start_sales_date, :end_sales_date, :is_paused, :is_hidden, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
