class AnglesController < ApplicationController
  before_action :set_angle, only: [:show, :edit, :update, :destroy]

  # GET /angles
  # GET /angles.json
  def index
    @angles = Angle.all
  end

  # GET /angles/1
  # GET /angles/1.json
  def show
  end

  # GET /angles/new
  def new
    @angle = Angle.new
  end

  # GET /angles/1/edit
  def edit
  end

  # POST /angles
  # POST /angles.json
  def create
    @angle = Angle.new(angle_params)

    respond_to do |format|
      if @angle.save
        format.html { redirect_to @angle, notice: 'Angle was successfully created.' }
        format.json { render :show, status: :created, location: @angle }
      else
        format.html { render :new }
        format.json { render json: @angle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /angles/1
  # PATCH/PUT /angles/1.json
  def update
    respond_to do |format|
      if @angle.update(angle_params)
        format.html { redirect_to @angle, notice: 'Angle was successfully updated.' }
        format.json { render :show, status: :ok, location: @angle }
      else
        format.html { render :edit }
        format.json { render json: @angle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /angles/1
  # DELETE /angles/1.json
  def destroy
    @angle.destroy
    respond_to do |format|
      format.html { redirect_to angles_url, notice: 'Angle was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_angle
      @angle = Angle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def angle_params
      params.require(:angle).permit(:name, :pricing, :state)
    end
end
