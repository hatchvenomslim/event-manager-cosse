class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy, :add_item, :remove_item, :purchase]

  before_action :authenticate_user!

  # GET /orders
  # GET /orders.json
  def index
    @orders = current_user.orders
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
    @ticket = Ticket.find(params[:ticket])
    @event = Event.friendly.find(params[:event])
    respond_to do |format|
      format.js
    end
  end

  def add_item
    if @order.update(quantity: @order.quantity + 1)
      respond_to do |format|
        format.js
      end
    end
  end

  def remove_item
    if @order.update(quantity: @order.quantity - 1)
      respond_to do |format|
        format.js
      end
    end
  end

  # GET /orders/1/edit
  def edit
  end

  def charges
    data = JSON.parse(request.body.read.to_s)
    @order = current_user.orders.where(id: params[:order_id].to_i).last
    if @order || data['payment_intent_id']
      amount = @order.amount * @order.quantity * 100 if @order

      begin
        if data['payment_method_id']
          # Create the PaymentIntent

          @intent = Stripe::PaymentIntent.create(
              payment_method: data['payment_method_id'],
              amount: amount.to_i,
              currency: 'eur',
              confirmation_method: 'manual',
              confirm: true,
              metadata: {order_id: @order.id}
          )
        elsif data['payment_intent_id']
          @intent = Stripe::PaymentIntent.confirm(data['payment_intent_id'])
        end
      rescue Stripe::CardError => e
        # Display error on client
        render json: {error: e.message}, status: 200
      end


      generate_payment_response(@intent)
    else
      render json: {error: 'bad request'}, status: :bad_request
    end

  end

  def payment
    @order = Order.find(params[:order_id])


    respond_to do |format|
      format.js
    end

  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params)

    respond_to do |format|
      if @order.save

        format.html {redirect_to @order}
        format.js {flash[:notice] = 'Order was successfully created.'}
        format.json {render :show, status: :created, location: @order}
      else
        format.html {render :new}
        format.js {render layout: false, content_type: 'text/javascript'}
        format.json {render json: @order.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html {redirect_to @order, notice: 'Order was successfully updated.'}
        format.json {render :show, status: :ok, location: @order}
      else
        format.html {render :edit}
        format.json {render json: @order.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order_id = @order.id
    @order.destroy
    respond_to do |format|
      format.html {redirect_to orders_url}
      format.js {flash[:notice] = 'Order was successfully destroyed.'}
      format.json {head :no_content}
    end


  end

  def purchase

    @order.update(status: 'payed')
    redirect_to order_items_path(@order)

  end

  private

  def generate_payment_response(intent)


    if intent.status == 'requires_source_action' &&
        intent.next_action.type == 'use_stripe_sdk'
      # Tell the client to handle the action
      render json: {requires_action: true, payment_intent_client_secret: intent.client_secret}, status: 200
    elsif intent.status == 'succeeded'
      order = Order.find(intent.metadata.order_id.to_i)
      order_ticket = order.ticket
      order.update(transaction_id: intent.id, status: 'payed')
      order_ticket.update(sold: order_ticket.sold_tickets)
      order.quantity.times do
        order.items.new(order: order, event: order.event)
        order.save
      end
      # The payment didn’t need any additional actions and completed!
      # Handle post-payment fulfillment
      #[200, { success: true }.to_json]
      render json: {success: true}, status: 200

    else
      # Invalid status
      #return [500, { error: 'Invalid PaymentIntent status' }.to_json]
      render json: {error: 'Invalid PaymentIntent status'}, status: 500
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:user_id, :ticket_id, :transaction_id, :amount, :event_id, :quantity)
  end
end
