class ExhibitorsController < ApplicationController
  before_action :set_exhibitor, only: [:show, :edit, :update, :destroy]
  # layout "eventster", only: [:new]

  # # GET /exhibitors
  # # GET /exhibitors.json
  # def index
  #   @exhibitors = Exhibitor.all
  # end

  # # GET /exhibitors/1
  # # GET /exhibitors/1.json
  # def show
  # end

  # GET /exhibitors/new
  def new
    @event = Event.friendly.find(params[:event_id])
    @exhibitor = Exhibitor.new
  end

  # GET /exhibitors/1/edit
  # def edit

  #   @event = Event.find(@exhibitor.event_id)
  # end

  # POST /exhibitors
  # POST /exhibitors.json
  def create
    @exhibitor = Exhibitor.new(exhibitor_params)

    respond_to do |format|
      if @exhibitor.save
        format.html { redirect_to event_path(@exhibitor.event_id), notice: 'Exhibitor was successfully created.' }
        format.json { render :show, status: :created, location: @exhibitor }
      else
        format.html { render :new }
        format.json { render json: @exhibitor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exhibitors/1
  # PATCH/PUT /exhibitors/1.json
  # def update
  #   respond_to do |format|
  #     if @exhibitor.update(exhibitor_params)
  #       format.html { redirect_to @exhibitor, notice: 'Exhibitor was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @exhibitor }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @exhibitor.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /exhibitors/1
  # # DELETE /exhibitors/1.json
  # def destroy
  #   @exhibitor.destroy
  #   respond_to do |format|
  #     format.html { redirect_to exhibitors_url, notice: 'Exhibitor was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exhibitor
      @exhibitor = Exhibitor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exhibitor_params
      params.require(:exhibitor).permit(:kbis,
                                        :avis,
                                        :artisan_inscription,
                                        :accepted_cgv,
                                        :is_exhibitor,
                                        :is_angle_a,
                                        :is_angle_b,
                                        :is_angle_c,
                                        :is_angle_d,
                                        :social_name,
                                        :address,
                                        :zipcode,
                                        :country_id,
                                        :phone,
                                        :fax,
                                        :web,
                                        :manager_name,
                                        :manager_phone,
                                        :manager_email,
                                        :commercial_name,
                                        :commercial_phone,
                                        :commercial_email,
                                        :marketing_name,
                                        :marketing_phone,
                                        :marketing_email,
                                        :email,
                                        :naf,
                                        :siren,
                                        :siret,
                                        :tva_code,
                                        :company_creation_date,
                                        :is_experienced,
                                        :experience_event,
                                        :experience_year,
                                        :contact_name,
                                        :contact_title,
                                        :contact_phone,
                                        :contact_mobile,
                                        :contact_address_type,
                                        :contact_address,
                                        :contact_email,
                                        :billing_address_type,
                                        :billing_address,
                                        :signage,
                                        :other_activity,
                                        :products,
                                        :brands,
                                        :insurance_agreement,
                                        :insurance_type,
                                        :booth_surface,
                                        :booth_junk_surface,
                                        :majoration_central_allery,
                                        :confort_surface,
                                        :turnkey_surface,
                                        :turnkey_options,
                                        :badges_count,
                                        :invitations_count,
                                        :logo_confirmed,
                                        :home_ad_banner,
                                        :visitor_ad_banner,
                                        :page_ad_banner,
                                        :summit_logo_confirmed,
                                        :bag_confirmed,
                                        :payment_version,
                                        :wishes,
                                        :event_id,
                                        :angle_id, city_ids: [])
    end
end
