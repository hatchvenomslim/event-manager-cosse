module Api
  class EventsController < BaseController
    def index
      @events = Event.all
    end
  end
end