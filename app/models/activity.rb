class Activity < ApplicationRecord
  belongs_to :event
  # belongs_to :exhibitor
  include PgSearch
  pg_search_scope :pg_search, against: [:name]


  has_many :affiliations
  has_many :exhibitors, through: :affiliations
end
