class Stand < ApplicationRecord
  belongs_to :event

  validates :stand_surface_price,
            presence: {
                message: "le prix doit être present"
            },
            numericality: {greater_than: 0, message: "Le prix doit être supérieur à Zéro!"}

  validates :stand_trash_price,
            presence: {
                message: "le prix doit être present"
            },
            numericality: {greater_than: 0, message: "Le prix doit être supérieur à Zéro!"}

  validates :stand_increase,
            presence: {
                message: "le prix doit être present"
            },
            numericality: {greater_than: 0, message: "Le prix doit être supérieur à Zéro!"}

  validates :stand_electricity_price,
            presence: {
                message: "le prix doit être present"
            },
            numericality: {greater_than: 0, message: "Le prix doit être supérieur à Zéro!"}
end
