class CommunicationTool < ApplicationRecord
  belongs_to :event

  validates :logo_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }

  validates :homepage_banner_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }


  validates :visitor_banner_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }

  validates :webpage_banner_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }

  validates :expo_logo_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }

  validates :distribution_bag_price, presence: {message: "le prix doit être present"}, numericality: {
      greater_than: 0,
      message: "Le prix doit être supérieur à Zéro!"
  }

end
