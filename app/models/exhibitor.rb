class Exhibitor < ApplicationRecord
    belongs_to :event
    # has_many :activities

    has_one_attached :kbis
    has_one_attached :avis
    has_one_attached :artisan_inscription


  has_many :affiliations
  has_many :activities, through: :affiliations
end
