class Affiliation < ApplicationRecord
  belongs_to :exhibitor
  belongs_to :activity
end
