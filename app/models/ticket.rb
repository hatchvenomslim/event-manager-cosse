class Ticket < ApplicationRecord
  belongs_to :event
  has_many :orders


  def available_qte
    quantity - sold_tickets
  end

  def sold_tickets
    orders.where(status: "payed").sum(:quantity)
  end

end
