class Event < ApplicationRecord
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::NumberHelper

  include PgSearch
  pg_search_scope :pg_search, against: [:name] #, using: { tsearch: { prefix: true, tsvector_column: "tsv" } }

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_one_attached :logo
  has_one_attached :cover

  has_many :exhibitors
  has_many :tickets
  has_many :items
  has_one :stand, dependent: :destroy
  has_one :local_layout, dependent: :destroy
  has_one :badge, dependent: :destroy
  has_one :communication_tool, dependent: :destroy

  accepts_nested_attributes_for :stand
  accepts_nested_attributes_for :local_layout
  accepts_nested_attributes_for :badge
  accepts_nested_attributes_for :communication_tool

  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :date_limit, presence: true
  validates :organizer_url, url: true
  validates :web_url, url: true
  validates :organizer_name, presence: true
  validate :date_validation

  scope :sort_date, -> { order(start_date: :asc) }

  def event_logo
    rails_blob_path(logo, only_path: true) if logo.attached?
  end

  def logo_url
    "#{request.protocol}#{request.host}"
  end

  def event_cover
    rails_blob_path(cover, only_path: true) if cover.attached?
  end

  def estimated_CA
  end

  def capital
    number_to_currency(target_sales, precision: 0, unit: "€", separator: ",", delimiter: " ", locale: :fr)
  end

  def date_validation
    if self[:end_date] < self[:start_date]
      errors[:end_date] << "La date du début doit être superieur à la date de fin"
      return false
    else
      return true
    end
  end
end
