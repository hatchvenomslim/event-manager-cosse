class Badge < ApplicationRecord
  belongs_to :event

  validates :badge_price,
            presence:
                {
                    message: "le prix doit être present"
                },
            numericality:
                {
                    greater_than: 0,
                    message: "Le prix doit être supérieur à Zéro!"
                }

  validates :invitation_card_price,
            presence:
                {
                    message: "le prix doit être present"
                },
            numericality:
                {
                    greater_than: 0,
                    message: "Le prix doit être supérieur à Zéro!"
                }
end
