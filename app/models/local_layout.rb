class LocalLayout < ApplicationRecord
  belongs_to :event

  validates :comfort_formula_price,
            presence:
                {
                    message: "le prix doit être present"
                },
            numericality:
                {
                    greater_than: 0,
                    message: "Le prix doit être supérieur à Zéro!"
                }

  validates :turnkey_formula_price,
            presence:
                {
                    message: "le prix doit être present"
                },
            numericality:
                {
                    greater_than: 0,
                    message: "Le prix doit être supérieur à Zéro!"
                }

end
