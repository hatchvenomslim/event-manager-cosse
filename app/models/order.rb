class Order < ApplicationRecord

  belongs_to :event
  belongs_to :user
  belongs_to :ticket

  has_many :items, dependent: :destroy

  validate :check_tickets_availability

  def total_price
    amount * quantity
  end
  private

  def check_tickets_availability
    if quantity < ticket.min_per_person
      errors.add(:base, "Il faut acheter au moins #{ticket.min_per_person} tickets!")
    end
    if quantity > ticket.max_per_person
      errors.add(:base, "Seulement #{ticket.max_per_person} tickets sont disponibles par personne!")
    end
  end


end
