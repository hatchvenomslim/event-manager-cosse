Trestle.resource(:events) do
  active_storage_fields do
    [:logo]
  end

  search do |query|
    query ? collection.pg_search(query) : collection
  end

  menu do
    item :events, icon: "fa fa-star", priority: :first
  end

  collection do
    model.sort_date
  end

  # menu do
  #   group :group_name, priority: :last do
  #     item :resource, icon: "fa fa-star", priority: :first
  #   end
  # end

  # Define custom scopes for the index view
  scopes do
    scope :all, default: true
    # scope :state
    # scope :drafts, -> { Event.unpublished }
  end

  # Define the index view table listing
  table do
    column :logo, header: nil, align: :center, class: "poster-column" do |event|
      admin_link_to(image_tag(event.event_logo, class: "small-img"), event) if event.logo.attached?
    end
    column :name, link: true, sort: :name do |event|
      safe_join([
                  content_tag(:strong, event.name),
                ])
    end
    column :capital, align: :center do |event|
      event.capital
    end
    column :capital_realized, align: :center do |event|
      "N.A"
    end
    column :exposants, align: :center do |event|
      event.exhibitors.count
    end
    column "Surface", align: :center do |event|
      event.exhibitors.sum(:booth_junk_surface)
    end
    column "Tickets", align: :center do |event|
      event.items.count
    end
    column :start_date, align: :center
    column :end_date, align: :center
    column :state, align: :center do |event|
      trestle_form_for(event, url: admin.instance_path(event, action: :update), data: { behavior: "live-checkbox" }) do |f|
        f.check_box :state, label: "Actif"
      end
    end
    actions
  end

  # Define the form structure for the new & edit actions
  form do |event|
    # Organize fields into tabs and sidebars
    tab "Événement" do
      sidebar do
        # form_group :logo, label: false do
        #   link_to image_tag(event.event_logo), event.event_logo, data: {behavior: "zoom"}
        # end if event.logo.attached?
        form_group :logo, help: "Dimensions: 360px par 100px - Taille: 2 Mo" do
          concat image_tag(event.event_logo, width: "100") if event.logo.attached?
          raw_file_field :logo
        end

        form_group :cover, help: "Dimensions: 780px par 400px - Taille: 2 Mo" do
          concat image_tag(event.event_cover) if event.cover.attached?
          raw_file_field :cover
        end
      end
      text_field :name
      text_area :description, rows: "5"
      row do
        col(sm: 6) { date_field :start_date }
        col(sm: 6) { date_field :end_date }
      end
      row do
        col(sm: 6) { text_field :web_url }
        col(sm: 6) { text_field :location }
        col(sm: 6) { date_field :date_limit }
      end

      number_field :target_sales
      row do
        col(sm: 6) { text_field :organizer_name }
        col(sm: 6) { text_field :organizer_url }
      end
      row do
        col(sm: 6) { text_field :facebook_link }
        col(sm: 6) { text_field :twitter_link }
        col(sm: 6) { text_field :instagram_link }
        col(sm: 6) { text_field :pinterest_link }
      end
      editor :cgv, label: "CGV"
      check_box :state
    end

    tab :stand do
      fields_for :stand, event.stand || event.build_stand do
        row do
          col(sm: 6) { number_field :stand_surface_price, label: "Prix stand m2", min: "0" }
          col(sm: 6) { number_field :stand_trash_price, label: "Prix m2 de l'élimination des déchets", min: "0", step: "0.01" }
        end

        row do
          col(sm: 6) { number_field :stand_electricity_price, label: "Prix de l'éléctricité", min: "0", step: "0.01" }
          col(sm: 6) { number_field :stand_increase, label: "Majoration", min: "0" }
        end
      end
    end

    tab "Aménagement" do
      fields_for :local_layout, event.local_layout || event.build_local_layout do
        col(sm: 6) { number_field :comfort_formula_price, label: "Prix formule confort", min: "0", step: "0.01" }
        col(sm: 6) { number_field :turnkey_formula_price, label: "Prix formule clé en main", min: "0", step: "0.01" }
      end
    end

    tab "Badges" do
      fields_for :badge, event.badge || event.build_badge do
        col(sm: 6) { number_field :badge_price, label: "Prix badge exposant supplémentaire", min: "0", step: "0.01" }
        col(sm: 6) { number_field :invitation_card_price, label: "Prix carte d'invitation", min: "0", step: "0.01" }
      end
    end

    tab "Communication" do
      fields_for :communication_tool, event.communication_tool || event.build_communication_tool do
        row do
          col(sm: 6) { number_field :logo_price, label: "Prix logo", min: "0", step: "0.01" }
          col(sm: 6) { number_field :homepage_banner_price, label: "Prix bannière en page d’accueil", min: "0", step: "0.01" }
        end

        row do
          col(sm: 6) { number_field :visitor_banner_price, label: "Prix bannière sur la page visiteur", min: "0", step: "0.01" }
          col(sm: 6) { number_field :webpage_banner_price, label: "Prix bannière sur une page au choix", min: "0", step: "0.01" }
        end

        row do
          col(sm: 6) { number_field :expo_logo_price, label: "Prix logo sur le plan de visite", min: "0", step: "0.01" }
          col(sm: 6) { number_field :distribution_bag_price, label: "Prix distribution de sac", min: "0", step: "0.01" }
        end
      end
    end

    # tab :metadata do
    #   # Layout fields based on a 12-column grid
    #   row do
    #     col(sm: 6) { select :author, User.all }
    #     col(sm: 6) { tag_select :tags }
    #   end
    # end

    # sidebar do
    #   # Render a custom partial: app/views/admin/posts/_sidebar.html.erb
    #   render "sidebar"
    # end
  end

  # Customize the table columns shown on the index view.
  #
  # table do
  #   column :name
  #   column :created_at, align: :center
  #   actions
  # end

  # Customize the form fields shown on the new/edit views.
  #
  # form do |event|
  #   text_field :name
  #
  #   row do
  #     col(xs: 6) { datetime_field :updated_at }
  #     col(xs: 6) { datetime_field :created_at }
  #   end
  # end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  # params do |params|
  #   params.require(:event).permit(:name, ...)
  # end
end
