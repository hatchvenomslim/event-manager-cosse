// This file may be used for providing additional customizations to the Trestle
// admin. It will be automatically included within all admin pages.
//
// For organizational purposes, you may wish to define your customizations
// within individual partials and `require` them here.
//
//  e.g. //= require "trestle/custom/my_custom_js"
//= require "trestle/custom/flat"

$(document).on('click', '[data-behavior="live-checkbox"]', function(e) {
    e.stopPropagation();
  
    $.ajax({
      type: "POST",
      url: $(this).attr('action'),
      data: $(this).serialize(),
      dataType: "script"
    });
  });