// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function () {
    jQuery(document).ready(function(){
        jQuery(this).scrollTop(0);
    });
    var notice = document.getElementById('notice');
    var alert = document.getElementById('alert');

    if (notice) {
        fade(notice);
    }

    if (alert) {
        fade(notice);
    }


});

function fade(el) {

    var opacity;
    if (el.style.opacity > 0.9) {
        el.style.opacity = 1;
        var myvar = setInterval(function () {
            el.style.opacity -= 0.05;
            if (el.style.opacity < 0) {
                clearInterval(myvar);
                opacity = Number(el.style.opacity);

                el.style.opacity = opacity;
            }
        }, 20);
    } else if (el.style.opacity < 1) {
        el.style.opacity = 0;

        var newvar = setInterval(function () {
            el.style.opacity = +parseFloat(el.style.opacity) + 0.05;
            if (el.style.opacity > 1) {
                clearInterval(newvar);
                opacity = Number(el.style.opacity);

                el.style.opacity = opacity;
            }
        }, 20);
    }
    setTimeout(function () {
        el.style.opacity = 0.5;
        el.remove();
    }, 3000);
    return;
}