require 'test_helper'

class ExhibitorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exhibitor = exhibitors(:one)
  end

  test "should get index" do
    get exhibitors_url
    assert_response :success
  end

  test "should get new" do
    get new_exhibitor_url
    assert_response :success
  end

  test "should create exhibitor" do
    assert_difference('Exhibitor.count') do
      post exhibitors_url, params: { exhibitor: { address: @exhibitor.address, angle_id: @exhibitor.angle_id, badges_count: @exhibitor.badges_count, bag_confirmed: @exhibitor.bag_confirmed, billing_address: @exhibitor.billing_address, billing_address_type: @exhibitor.billing_address_type, booth_junk_surface: @exhibitor.booth_junk_surface, booth_surface: @exhibitor.booth_surface, brands: @exhibitor.brands, commercial_email: @exhibitor.commercial_email, commercial_name: @exhibitor.commercial_name, commercial_phone: @exhibitor.commercial_phone, company_creation_date: @exhibitor.company_creation_date, confort_surface: @exhibitor.confort_surface, contact_address: @exhibitor.contact_address, contact_address_type: @exhibitor.contact_address_type, contact_email: @exhibitor.contact_email, contact_mobile: @exhibitor.contact_mobile, contact_name: @exhibitor.contact_name, contact_phone: @exhibitor.contact_phone, contact_title: @exhibitor.contact_title, country_id: @exhibitor.country_id, email: @exhibitor.email, event_id: @exhibitor.event_id, experience_event: @exhibitor.experience_event, experience_year: @exhibitor.experience_year, fax: @exhibitor.fax, home_ad_banner: @exhibitor.home_ad_banner, insurance_agreement: @exhibitor.insurance_agreement, insurance_type: @exhibitor.insurance_type, invitations_count: @exhibitor.invitations_count, is_exhibitor: @exhibitor.is_exhibitor, is_experienced: @exhibitor.is_experienced, logo_confirmed: @exhibitor.logo_confirmed, majoration_central_allery: @exhibitor.majoration_central_allery, manager_email: @exhibitor.manager_email, manager_name: @exhibitor.manager_name, manager_phone: @exhibitor.manager_phone, marketing_email: @exhibitor.marketing_email, marketing_name: @exhibitor.marketing_name, marketing_phone: @exhibitor.marketing_phone, naf: @exhibitor.naf, other_activity: @exhibitor.other_activity, page_ad_banner: @exhibitor.page_ad_banner, payment_version: @exhibitor.payment_version, phone: @exhibitor.phone, products: @exhibitor.products, signage: @exhibitor.signage, siren: @exhibitor.siren, social_name: @exhibitor.social_name, summit_logo_confirmed: @exhibitor.summit_logo_confirmed, turnkey_options: @exhibitor.turnkey_options, turnkey_surface: @exhibitor.turnkey_surface, tva_code: @exhibitor.tva_code, visitor_ad_banner: @exhibitor.visitor_ad_banner, web: @exhibitor.web, wishes: @exhibitor.wishes, zipcode: @exhibitor.zipcode } }
    end

    assert_redirected_to exhibitor_url(Exhibitor.last)
  end

  test "should show exhibitor" do
    get exhibitor_url(@exhibitor)
    assert_response :success
  end

  test "should get edit" do
    get edit_exhibitor_url(@exhibitor)
    assert_response :success
  end

  test "should update exhibitor" do
    patch exhibitor_url(@exhibitor), params: { exhibitor: { address: @exhibitor.address, angle_id: @exhibitor.angle_id, badges_count: @exhibitor.badges_count, bag_confirmed: @exhibitor.bag_confirmed, billing_address: @exhibitor.billing_address, billing_address_type: @exhibitor.billing_address_type, booth_junk_surface: @exhibitor.booth_junk_surface, booth_surface: @exhibitor.booth_surface, brands: @exhibitor.brands, commercial_email: @exhibitor.commercial_email, commercial_name: @exhibitor.commercial_name, commercial_phone: @exhibitor.commercial_phone, company_creation_date: @exhibitor.company_creation_date, confort_surface: @exhibitor.confort_surface, contact_address: @exhibitor.contact_address, contact_address_type: @exhibitor.contact_address_type, contact_email: @exhibitor.contact_email, contact_mobile: @exhibitor.contact_mobile, contact_name: @exhibitor.contact_name, contact_phone: @exhibitor.contact_phone, contact_title: @exhibitor.contact_title, country_id: @exhibitor.country_id, email: @exhibitor.email, event_id: @exhibitor.event_id, experience_event: @exhibitor.experience_event, experience_year: @exhibitor.experience_year, fax: @exhibitor.fax, home_ad_banner: @exhibitor.home_ad_banner, insurance_agreement: @exhibitor.insurance_agreement, insurance_type: @exhibitor.insurance_type, invitations_count: @exhibitor.invitations_count, is_exhibitor: @exhibitor.is_exhibitor, is_experienced: @exhibitor.is_experienced, logo_confirmed: @exhibitor.logo_confirmed, majoration_central_allery: @exhibitor.majoration_central_allery, manager_email: @exhibitor.manager_email, manager_name: @exhibitor.manager_name, manager_phone: @exhibitor.manager_phone, marketing_email: @exhibitor.marketing_email, marketing_name: @exhibitor.marketing_name, marketing_phone: @exhibitor.marketing_phone, naf: @exhibitor.naf, other_activity: @exhibitor.other_activity, page_ad_banner: @exhibitor.page_ad_banner, payment_version: @exhibitor.payment_version, phone: @exhibitor.phone, products: @exhibitor.products, signage: @exhibitor.signage, siren: @exhibitor.siren, social_name: @exhibitor.social_name, summit_logo_confirmed: @exhibitor.summit_logo_confirmed, turnkey_options: @exhibitor.turnkey_options, turnkey_surface: @exhibitor.turnkey_surface, tva_code: @exhibitor.tva_code, visitor_ad_banner: @exhibitor.visitor_ad_banner, web: @exhibitor.web, wishes: @exhibitor.wishes, zipcode: @exhibitor.zipcode } }
    assert_redirected_to exhibitor_url(@exhibitor)
  end

  test "should destroy exhibitor" do
    assert_difference('Exhibitor.count', -1) do
      delete exhibitor_url(@exhibitor)
    end

    assert_redirected_to exhibitors_url
  end
end
