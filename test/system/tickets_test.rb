require "application_system_test_case"

class TicketsTest < ApplicationSystemTestCase
  setup do
    @ticket = tickets(:one)
  end

  test "visiting the index" do
    visit tickets_url
    assert_selector "h1", text: "Tickets"
  end

  test "creating a Ticket" do
    visit tickets_url
    click_on "New Ticket"

    fill_in "End sales date", with: @ticket.end_sales_date
    fill_in "Event", with: @ticket.event_id
    check "Is hidden" if @ticket.is_hidden
    check "Is paused" if @ticket.is_paused
    fill_in "Max per person", with: @ticket.max_per_person
    fill_in "Min per person", with: @ticket.min_per_person
    fill_in "Price", with: @ticket.price
    fill_in "Quantity", with: @ticket.quantity
    fill_in "Sold", with: @ticket.sold
    fill_in "Start sales date", with: @ticket.start_sales_date
    fill_in "Title", with: @ticket.title
    click_on "Create Ticket"

    assert_text "Ticket was successfully created"
    click_on "Back"
  end

  test "updating a Ticket" do
    visit tickets_url
    click_on "Edit", match: :first

    fill_in "End sales date", with: @ticket.end_sales_date
    fill_in "Event", with: @ticket.event_id
    check "Is hidden" if @ticket.is_hidden
    check "Is paused" if @ticket.is_paused
    fill_in "Max per person", with: @ticket.max_per_person
    fill_in "Min per person", with: @ticket.min_per_person
    fill_in "Price", with: @ticket.price
    fill_in "Quantity", with: @ticket.quantity
    fill_in "Sold", with: @ticket.sold
    fill_in "Start sales date", with: @ticket.start_sales_date
    fill_in "Title", with: @ticket.title
    click_on "Update Ticket"

    assert_text "Ticket was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket" do
    visit tickets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket was successfully destroyed"
  end
end
