require "application_system_test_case"

class AnglesTest < ApplicationSystemTestCase
  setup do
    @angle = angles(:one)
  end

  test "visiting the index" do
    visit angles_url
    assert_selector "h1", text: "Angles"
  end

  test "creating a Angle" do
    visit angles_url
    click_on "New Angle"

    fill_in "Name", with: @angle.name
    fill_in "Pricing", with: @angle.pricing
    check "State" if @angle.state
    click_on "Create Angle"

    assert_text "Angle was successfully created"
    click_on "Back"
  end

  test "updating a Angle" do
    visit angles_url
    click_on "Edit", match: :first

    fill_in "Name", with: @angle.name
    fill_in "Pricing", with: @angle.pricing
    check "State" if @angle.state
    click_on "Update Angle"

    assert_text "Angle was successfully updated"
    click_on "Back"
  end

  test "destroying a Angle" do
    visit angles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Angle was successfully destroyed"
  end
end
