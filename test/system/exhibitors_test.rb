require "application_system_test_case"

class ExhibitorsTest < ApplicationSystemTestCase
  setup do
    @exhibitor = exhibitors(:one)
  end

  test "visiting the index" do
    visit exhibitors_url
    assert_selector "h1", text: "Exhibitors"
  end

  test "creating a Exhibitor" do
    visit exhibitors_url
    click_on "New Exhibitor"

    fill_in "Address", with: @exhibitor.address
    fill_in "Angle", with: @exhibitor.angle_id
    fill_in "Badges count", with: @exhibitor.badges_count
    check "Bag confirmed" if @exhibitor.bag_confirmed
    fill_in "Billing address", with: @exhibitor.billing_address
    fill_in "Billing address type", with: @exhibitor.billing_address_type
    fill_in "Booth junk surface", with: @exhibitor.booth_junk_surface
    fill_in "Booth surface", with: @exhibitor.booth_surface
    fill_in "Brands", with: @exhibitor.brands
    fill_in "Commercial email", with: @exhibitor.commercial_email
    fill_in "Commercial name", with: @exhibitor.commercial_name
    fill_in "Commercial phone", with: @exhibitor.commercial_phone
    fill_in "Company creation date", with: @exhibitor.company_creation_date
    fill_in "Confort surface", with: @exhibitor.confort_surface
    fill_in "Contact address", with: @exhibitor.contact_address
    fill_in "Contact address type", with: @exhibitor.contact_address_type
    fill_in "Contact email", with: @exhibitor.contact_email
    fill_in "Contact mobile", with: @exhibitor.contact_mobile
    fill_in "Contact name", with: @exhibitor.contact_name
    fill_in "Contact phone", with: @exhibitor.contact_phone
    fill_in "Contact title", with: @exhibitor.contact_title
    fill_in "Country", with: @exhibitor.country_id
    fill_in "Email", with: @exhibitor.email
    fill_in "Event", with: @exhibitor.event_id
    fill_in "Experience event", with: @exhibitor.experience_event
    fill_in "Experience year", with: @exhibitor.experience_year
    fill_in "Fax", with: @exhibitor.fax
    check "Home ad banner" if @exhibitor.home_ad_banner
    check "Insurance agreement" if @exhibitor.insurance_agreement
    fill_in "Insurance type", with: @exhibitor.insurance_type
    fill_in "Invitations count", with: @exhibitor.invitations_count
    check "Is exhibitor" if @exhibitor.is_exhibitor
    check "Is experienced" if @exhibitor.is_experienced
    check "Logo confirmed" if @exhibitor.logo_confirmed
    fill_in "Majoration central allery", with: @exhibitor.majoration_central_allery
    fill_in "Manager email", with: @exhibitor.manager_email
    fill_in "Manager name", with: @exhibitor.manager_name
    fill_in "Manager phone", with: @exhibitor.manager_phone
    fill_in "Marketing email", with: @exhibitor.marketing_email
    fill_in "Marketing name", with: @exhibitor.marketing_name
    fill_in "Marketing phone", with: @exhibitor.marketing_phone
    fill_in "Naf", with: @exhibitor.naf
    fill_in "Other activity", with: @exhibitor.other_activity
    check "Page ad banner" if @exhibitor.page_ad_banner
    fill_in "Payment version", with: @exhibitor.payment_version
    fill_in "Phone", with: @exhibitor.phone
    fill_in "Products", with: @exhibitor.products
    fill_in "Signage", with: @exhibitor.signage
    fill_in "Siren", with: @exhibitor.siren
    fill_in "Social name", with: @exhibitor.social_name
    check "Summit logo confirmed" if @exhibitor.summit_logo_confirmed
    fill_in "Turnkey options", with: @exhibitor.turnkey_options
    fill_in "Turnkey surface", with: @exhibitor.turnkey_surface
    fill_in "Tva code", with: @exhibitor.tva_code
    check "Visitor ad banner" if @exhibitor.visitor_ad_banner
    fill_in "Web", with: @exhibitor.web
    fill_in "Wishes", with: @exhibitor.wishes
    fill_in "Zipcode", with: @exhibitor.zipcode
    click_on "Create Exhibitor"

    assert_text "Exhibitor was successfully created"
    click_on "Back"
  end

  test "updating a Exhibitor" do
    visit exhibitors_url
    click_on "Edit", match: :first

    fill_in "Address", with: @exhibitor.address
    fill_in "Angle", with: @exhibitor.angle_id
    fill_in "Badges count", with: @exhibitor.badges_count
    check "Bag confirmed" if @exhibitor.bag_confirmed
    fill_in "Billing address", with: @exhibitor.billing_address
    fill_in "Billing address type", with: @exhibitor.billing_address_type
    fill_in "Booth junk surface", with: @exhibitor.booth_junk_surface
    fill_in "Booth surface", with: @exhibitor.booth_surface
    fill_in "Brands", with: @exhibitor.brands
    fill_in "Commercial email", with: @exhibitor.commercial_email
    fill_in "Commercial name", with: @exhibitor.commercial_name
    fill_in "Commercial phone", with: @exhibitor.commercial_phone
    fill_in "Company creation date", with: @exhibitor.company_creation_date
    fill_in "Confort surface", with: @exhibitor.confort_surface
    fill_in "Contact address", with: @exhibitor.contact_address
    fill_in "Contact address type", with: @exhibitor.contact_address_type
    fill_in "Contact email", with: @exhibitor.contact_email
    fill_in "Contact mobile", with: @exhibitor.contact_mobile
    fill_in "Contact name", with: @exhibitor.contact_name
    fill_in "Contact phone", with: @exhibitor.contact_phone
    fill_in "Contact title", with: @exhibitor.contact_title
    fill_in "Country", with: @exhibitor.country_id
    fill_in "Email", with: @exhibitor.email
    fill_in "Event", with: @exhibitor.event_id
    fill_in "Experience event", with: @exhibitor.experience_event
    fill_in "Experience year", with: @exhibitor.experience_year
    fill_in "Fax", with: @exhibitor.fax
    check "Home ad banner" if @exhibitor.home_ad_banner
    check "Insurance agreement" if @exhibitor.insurance_agreement
    fill_in "Insurance type", with: @exhibitor.insurance_type
    fill_in "Invitations count", with: @exhibitor.invitations_count
    check "Is exhibitor" if @exhibitor.is_exhibitor
    check "Is experienced" if @exhibitor.is_experienced
    check "Logo confirmed" if @exhibitor.logo_confirmed
    fill_in "Majoration central allery", with: @exhibitor.majoration_central_allery
    fill_in "Manager email", with: @exhibitor.manager_email
    fill_in "Manager name", with: @exhibitor.manager_name
    fill_in "Manager phone", with: @exhibitor.manager_phone
    fill_in "Marketing email", with: @exhibitor.marketing_email
    fill_in "Marketing name", with: @exhibitor.marketing_name
    fill_in "Marketing phone", with: @exhibitor.marketing_phone
    fill_in "Naf", with: @exhibitor.naf
    fill_in "Other activity", with: @exhibitor.other_activity
    check "Page ad banner" if @exhibitor.page_ad_banner
    fill_in "Payment version", with: @exhibitor.payment_version
    fill_in "Phone", with: @exhibitor.phone
    fill_in "Products", with: @exhibitor.products
    fill_in "Signage", with: @exhibitor.signage
    fill_in "Siren", with: @exhibitor.siren
    fill_in "Social name", with: @exhibitor.social_name
    check "Summit logo confirmed" if @exhibitor.summit_logo_confirmed
    fill_in "Turnkey options", with: @exhibitor.turnkey_options
    fill_in "Turnkey surface", with: @exhibitor.turnkey_surface
    fill_in "Tva code", with: @exhibitor.tva_code
    check "Visitor ad banner" if @exhibitor.visitor_ad_banner
    fill_in "Web", with: @exhibitor.web
    fill_in "Wishes", with: @exhibitor.wishes
    fill_in "Zipcode", with: @exhibitor.zipcode
    click_on "Update Exhibitor"

    assert_text "Exhibitor was successfully updated"
    click_on "Back"
  end

  test "destroying a Exhibitor" do
    visit exhibitors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Exhibitor was successfully destroyed"
  end
end
