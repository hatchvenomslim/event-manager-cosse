# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_02_144314) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "activities", force: :cascade do |t|
    t.string "name"
    t.boolean "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id"
    t.index ["event_id"], name: "index_activities_on_event_id"
  end

  create_table "administrators", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "remember_token"
    t.datetime "remember_token_expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affiliations", force: :cascade do |t|
    t.integer "exhibitor_id"
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_id"], name: "index_affiliations_on_activity_id"
    t.index ["exhibitor_id"], name: "index_affiliations_on_exhibitor_id"
  end

  create_table "angles", force: :cascade do |t|
    t.string "name"
    t.integer "pricing"
    t.boolean "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "badges", force: :cascade do |t|
    t.integer "event_id"
    t.float "badge_price"
    t.float "invitation_card_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_badges_on_event_id"
  end

  create_table "communication_tools", force: :cascade do |t|
    t.integer "event_id"
    t.float "logo_price"
    t.float "homepage_banner_price"
    t.float "visitor_banner_price"
    t.float "webpage_banner_price"
    t.float "expo_logo_price"
    t.float "distribution_bag_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_communication_tools_on_event_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.text "description"
    t.string "location"
    t.string "facebook_link"
    t.string "twitter_link"
    t.string "instagram_link"
    t.string "pinterest_link"
    t.boolean "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "slug"
    t.string "web_url"
    t.text "cgv"
    t.boolean "accepted_cgv"
    t.date "date_limit"
    t.integer "target_sales"
    t.time "start_hour"
    t.time "end_hour"
    t.string "organizer_name"
    t.string "organizer_url"
    t.index ["slug"], name: "index_events_on_slug", unique: true
  end

  create_table "exhibitors", force: :cascade do |t|
    t.boolean "is_exhibitor"
    t.string "social_name"
    t.string "address"
    t.string "zipcode"
    t.string "phone"
    t.string "fax"
    t.string "web"
    t.string "manager_name"
    t.string "manager_phone"
    t.string "manager_email"
    t.string "commercial_name"
    t.string "commercial_phone"
    t.string "commercial_email"
    t.string "marketing_name"
    t.string "marketing_phone"
    t.string "marketing_email"
    t.string "email"
    t.string "naf"
    t.string "siren"
    t.string "tva_code"
    t.date "company_creation_date"
    t.boolean "is_experienced"
    t.string "experience_event"
    t.integer "experience_year"
    t.string "contact_name"
    t.string "contact_title"
    t.string "contact_phone"
    t.string "contact_mobile"
    t.string "contact_address_type"
    t.string "contact_address"
    t.string "contact_email"
    t.string "billing_address_type"
    t.string "billing_address"
    t.string "signage"
    t.string "other_activity"
    t.text "products"
    t.text "brands"
    t.boolean "insurance_agreement"
    t.string "insurance_type"
    t.integer "booth_surface"
    t.integer "booth_junk_surface"
    t.integer "majoration_central_allery"
    t.integer "confort_surface"
    t.integer "turnkey_surface"
    t.string "turnkey_options"
    t.integer "badges_count"
    t.integer "invitations_count"
    t.boolean "logo_confirmed"
    t.boolean "home_ad_banner"
    t.boolean "visitor_ad_banner"
    t.boolean "page_ad_banner"
    t.boolean "summit_logo_confirmed"
    t.boolean "bag_confirmed"
    t.integer "payment_version"
    t.text "wishes"
    t.integer "event_id"
    t.integer "angle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "country"
    t.boolean "is_angle_a"
    t.boolean "is_angle_b"
    t.boolean "is_angle_c"
    t.boolean "is_angle_d"
    t.boolean "accepted_cgv"
    t.string "siret"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.integer "order_id"
    t.integer "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "local_layouts", force: :cascade do |t|
    t.integer "event_id"
    t.float "comfort_formula_price"
    t.float "turnkey_formula_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_local_layouts_on_event_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "user_id"
    t.integer "ticket_id"
    t.string "transaction_id"
    t.float "amount"
    t.integer "event_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
  end

  create_table "stands", force: :cascade do |t|
    t.integer "event_id"
    t.float "stand_surface_price"
    t.float "stand_trash_price"
    t.integer "stand_increase"
    t.float "stand_electricity_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_stands_on_event_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "title"
    t.integer "event_id"
    t.float "price"
    t.integer "max_per_person"
    t.integer "min_per_person"
    t.integer "quantity"
    t.integer "sold"
    t.date "start_sales_date"
    t.date "end_sales_date"
    t.boolean "is_paused"
    t.boolean "is_hidden"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
