class CreateAffiliations < ActiveRecord::Migration[5.2]
  def change
    create_table :affiliations do |t|
      t.references :exhibitor, index: true, foreign_key: true
      t.references :activity, index: true, foreign_key: true

      t.timestamps
    end
  end
end
