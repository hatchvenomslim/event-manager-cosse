class CreateStands < ActiveRecord::Migration[5.2]
  def change
    create_table :stands do |t|
      t.references :event, foreign_key: true
      t.float :stand_surface_price
      t.float :stand_trash_price
      t.integer :stand_increase
      t.float :stand_electricity_price

      t.timestamps
    end
  end
end
