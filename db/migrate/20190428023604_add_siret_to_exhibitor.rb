class AddSiretToExhibitor < ActiveRecord::Migration[5.2]
  def change
    add_column :exhibitors, :siret, :string
  end
end
