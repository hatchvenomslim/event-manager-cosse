class CreateAngles < ActiveRecord::Migration[5.2]
  def change
    create_table :angles do |t|
      t.string :name
      t.integer :pricing
      t.boolean :state

      t.timestamps
    end
  end
end
