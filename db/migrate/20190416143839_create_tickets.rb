class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :title
      t.integer :event_id
      t.float :price
      t.integer :max_per_person
      t.integer :min_per_person
      t.integer :quantity
      t.integer :sold
      t.date :start_sales_date
      t.date :end_sales_date
      t.boolean :is_paused
      t.boolean :is_hidden

      t.timestamps
    end
  end
end
