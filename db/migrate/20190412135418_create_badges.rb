class CreateBadges < ActiveRecord::Migration[5.2]
  def change
    create_table :badges do |t|
      t.references :event, foreign_key: true
      t.float :badge_price
      t.float :invitation_card_price

      t.timestamps
    end
  end
end
