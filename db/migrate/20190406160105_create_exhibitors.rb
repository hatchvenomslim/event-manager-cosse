class CreateExhibitors < ActiveRecord::Migration[5.2]
  def change
    create_table :exhibitors do |t|
      t.boolean :is_exhibitor
      t.string :social_name
      t.string :address
      t.string :zipcode
      t.integer :country_id
      t.string :phone
      t.string :fax
      t.string :web
      t.string :manager_name
      t.string :manager_phone
      t.string :manager_email
      t.string :commercial_name
      t.string :commercial_phone
      t.string :commercial_email
      t.string :marketing_name
      t.string :marketing_phone
      t.string :marketing_email
      t.string :email
      t.string :naf
      t.string :siren
      t.string :tva_code
      t.date :company_creation_date
      t.boolean :is_experienced
      t.string :experience_event
      t.integer :experience_year
      t.string :contact_name
      t.string :contact_title
      t.string :contact_phone
      t.string :contact_mobile
      t.string :contact_address_type
      t.string :contact_address
      t.string :contact_email
      t.string :billing_address_type
      t.string :billing_address
      t.string :signage
      t.string :other_activity
      t.text :products
      t.text :brands
      t.boolean :insurance_agreement
      t.string :insurance_type
      t.integer :booth_surface
      t.integer :booth_junk_surface
      t.integer :majoration_central_allery
      t.integer :confort_surface
      t.integer :turnkey_surface
      t.string :turnkey_options
      t.integer :badges_count
      t.integer :invitations_count
      t.boolean :logo_confirmed
      t.boolean :home_ad_banner
      t.boolean :visitor_ad_banner
      t.boolean :page_ad_banner
      t.boolean :summit_logo_confirmed
      t.boolean :bag_confirmed
      t.integer :payment_version
      t.text :wishes
      t.integer :event_id
      t.integer :angle_id

      t.timestamps
    end
  end
end
