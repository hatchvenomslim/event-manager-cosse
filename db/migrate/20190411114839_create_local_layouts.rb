class CreateLocalLayouts < ActiveRecord::Migration[5.2]
  def change
    create_table :local_layouts do |t|
      t.references :event, foreign_key: true
      t.float :comfort_formula_price
      t.float :turnkey_formula_price

      t.timestamps
    end
  end
end
