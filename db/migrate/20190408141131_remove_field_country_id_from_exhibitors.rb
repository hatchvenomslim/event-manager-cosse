class RemoveFieldCountryIdFromExhibitors < ActiveRecord::Migration[5.2]
  def change
    remove_column :exhibitors, :country_id, :integer
  end
end
