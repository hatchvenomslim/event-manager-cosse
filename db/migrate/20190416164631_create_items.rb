class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :order_id
      t.integer :event_id

      t.timestamps
    end
  end
end
