class AddTotalToExhibitor < ActiveRecord::Migration[5.2]
  def change
    add_column :exhibitors, :total, :float
  end
end
