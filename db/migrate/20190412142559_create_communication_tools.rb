class CreateCommunicationTools < ActiveRecord::Migration[5.2]
  def change
    create_table :communication_tools do |t|
      t.references :event, foreign_key: true
      t.float :logo_price
      t.float :homepage_banner_price
      t.float :visitor_banner_price
      t.float :webpage_banner_price
      t.float :expo_logo_price
      t.float :distribution_bag_price

      t.timestamps
    end
  end
end
