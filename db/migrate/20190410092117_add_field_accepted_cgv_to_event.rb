class AddFieldAcceptedCgvToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :accepted_cgv, :boolean
    add_column :events, :date_limit, :date
    add_column :events, :target_sales, :integer
  end
end
