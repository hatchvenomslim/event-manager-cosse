class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.text :description
      t.string :location
      t.string :facebook_link
      t.string :twitter_link
      t.string :instagram_link
      t.string :pinterest_link
      t.boolean :state

      t.timestamps
    end
  end
end
