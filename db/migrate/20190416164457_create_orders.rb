class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :ticket_id
      t.string :transaction_id
      t.float :amount
      t.integer :event_id
      t.integer :quantity

      t.timestamps
    end
  end
end
