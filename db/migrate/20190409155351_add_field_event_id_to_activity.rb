class AddFieldEventIdToActivity < ActiveRecord::Migration[5.2]
  def change
    add_reference :activities, :event, index: true
  end
end
