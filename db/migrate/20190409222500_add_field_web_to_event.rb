class AddFieldWebToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :web_url, :string
    add_column :events, :cgv, :text
  end
end
