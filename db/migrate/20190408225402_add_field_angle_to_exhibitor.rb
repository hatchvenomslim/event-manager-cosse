class AddFieldAngleToExhibitor < ActiveRecord::Migration[5.2]
  def change
    add_column :exhibitors, :is_angle_a, :boolean
    add_column :exhibitors, :is_angle_b, :boolean
    add_column :exhibitors, :is_angle_c, :boolean
    add_column :exhibitors, :is_angle_d, :boolean
  end
end
