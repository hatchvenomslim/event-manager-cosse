class AddAttributesToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :organizer_name, :string
    add_column :events, :organizer_url, :string
  end
end
