class AddFieldAcceptedCgvToExhibitors < ActiveRecord::Migration[5.2]
  def change
    add_column :exhibitors, :accepted_cgv, :boolean
  end
end
